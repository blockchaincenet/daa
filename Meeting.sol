pragma solidity >=0.7.0 <0.8.4;

import './Association.sol';
contract Meeting{

Association A;
address public associationAddress;

struct Voter {
    bool voted;
    bool votedYes;
    bool votedNo;
}

mapping (address => Voter) public voters;

enum VOTINGTYPE {BOARDMEETING, STATUTECHANGE, APPOINTBOARDMEMBER, DISMISSBOARDMEMBER, PURPOSECHANGE, LIQUIDATION, NEWMEMBER}
VOTINGTYPE public votingType;

string public votingDescription;
uint public voteYesCounter;
uint public voteNoCounter;
uint public abstentionsCounter;
uint public totalVotes;
uint public timeTillVotingExpiresInSeconds;
uint public timeTillVotingStartsInSeconds;

bool public confirmed;
uint public confirmationCounter;
mapping (address => bool) public confirmers;

constructor(VOTINGTYPE _votingType, string memory _votingDescription, uint _timeTillVotingStartsInSeconds, uint _timeTillMeetingExpiresInSeconds){
    A = Association(msg.sender);
    associationAddress = msg.sender;
    votingDescription = _votingDescription;
    votingType = _votingType;
    confirmationCounter = 0;
    voteYesCounter = 0;
    voteNoCounter = 0;
    timeTillVotingExpiresInSeconds = _timeTillMeetingExpiresInSeconds;
    timeTillVotingStartsInSeconds = _timeTillVotingStartsInSeconds;

    if(A.boardMemberMapping(address(tx.origin))){
        confirmed = true;
    }
    else{
        confirmed = false;
    }
}

modifier onlyAssociation{
    require(msg.sender == associationAddress, 'only association authorized.');
    _;
}

modifier onlyAuhorizedMember{
    if(votingType == VOTINGTYPE.BOARDMEETING){
        require(A.isBoardMember(msg.sender), 'sender is no board member.');
    }else {
        require(A.isMember(msg.sender), 'sender is no member.');
    }
    _;
}

modifier onlyWithinVotingTime{
    require(block.timestamp >= timeTillVotingStartsInSeconds, 'Voting has not started');
    require(block.timestamp <= timeTillVotingExpiresInSeconds, "Voting Time is over");
    _;
}
   
modifier votingIsOver{
    if(votingType == VOTINGTYPE.BOARDMEETING){
        if(A.amountAuthorizedBoardMembers() == totalVotes){
            _;
        }else{
        require((block.timestamp > timeTillVotingExpiresInSeconds) || (totalVotes == A.amountAuthorizedBoardMembers()), "Voting is not over yet.");
        _;
        }
    }else {
        if(A.amountAuthorizedMembers() == totalVotes){
            _;
        }else{
        require((block.timestamp > timeTillVotingExpiresInSeconds) || (totalVotes == A.amountAuthorizedMembers()), "Voting is not over yet.");
        _;
        }
    }
}

modifier isConfirmed {
    require(confirmed, 'Proposal not confirmed.');
    _;
}

modifier notVoted(){
    require(!voters[msg.sender].voted, 'Already voted.');
    _;
}
/*
*NEWMEMBER
*/

function setProposedNewMember(address _nm) onlyAssociation public {
    proposedNewMember = _nm;
}

address public proposedNewMember;
/*
* APPOINTBOARDMEMBER
*/
function setProposedBoardMember(address _pbm) onlyAssociation public {
    proposedCandidate.candidateAdress = _pbm;
    proposedCandidate.approved = false;
}

Candidate public proposedCandidate;

struct Candidate {
    address candidateAdress;
    bool approved;
}

modifier isApproved(){
    if(votingType == VOTINGTYPE.APPOINTBOARDMEMBER){
        require(proposedCandidate.approved, 'Candidate has not confirmed Voting .');
    }
    _;
}

/*
*CHANGE PURPOSE
*/
function setProposedPurpose(string memory _pp) onlyAssociation public{
    proposedPurpose = _pp;
}
string public proposedPurpose;

/*
*Change Statute
*/
struct ProposedStatute{
    uint statutePart;
    uint proposedValue;
}

function setProposedStatute(uint _sp, uint _pv) onlyAssociation public{
    proposedStatute.statutePart = _sp;
    proposedStatute.proposedValue = _pv;
}

ProposedStatute public proposedStatute;

/*
*Board Meeting
*/
struct ProposedBoardMeeting {
    address receiver;
    uint amountInWei;
}

function setProposedBoardMeeting(address _rec, uint _amountInWei) onlyAssociation public{
    proposedBoardMeeting.receiver = _rec;
    proposedBoardMeeting.amountInWei = _amountInWei;
}

ProposedBoardMeeting public proposedBoardMeeting;

function approveVoting() external {
    require(msg.sender == proposedCandidate.candidateAdress, 'only Candidate authorized.');
    proposedCandidate.approved = true;
}

function confirmProposal() onlyAuhorizedMember external {
    require(!confirmed, 'Proposal already confirmed.');
    require(!confirmers[msg.sender], 'you already confirmed');
    confirmationCounter++;
    confirmers[msg.sender] = true;
    if(confirmationCounter > calcPercentOf(uint(A.getStatuteData('minPercentageConfirmationForGM')), A.getTotalNumberMembers())){
        confirmed = true;
    }
   
}

function calcPercentOf(uint perc, uint ofNum) private pure returns(uint) {
    return (ofNum*perc)/100;
}

function voteYes() onlyAuhorizedMember onlyWithinVotingTime isConfirmed isApproved notVoted external {
        voters[msg.sender].votedYes = true;
        voters[msg.sender].votedNo = false;
        voters[msg.sender].voted = true;
        voteYesCounter++;
        totalVotes++;
        }

function voteNo() onlyAuhorizedMember onlyWithinVotingTime isConfirmed isApproved notVoted external {
        voters[msg.sender].votedYes = false;
        voters[msg.sender].votedNo = true;
        voters[msg.sender].voted = true;
        voteNoCounter++;
        totalVotes++;
        }

function containVote() onlyAuhorizedMember onlyWithinVotingTime isConfirmed isApproved notVoted external {
        voters[msg.sender].votedYes = false;
        voters[msg.sender].votedNo = false;
        voters[msg.sender].voted = true;
        abstentionsCounter++;
        totalVotes++;
        }

function executeResultOfMeeting() onlyAuhorizedMember votingIsOver external {
    if(votingType == VOTINGTYPE.BOARDMEETING){
        require(totalVotes >= uint(A.getStatuteData('minAmountQuorumBoardMeeting')), 'Quroum not reached.');
        require(voteYesCounter >= calcPercentOf(uint(A.getStatuteData('minPercentageBoardMemberVote')), totalVotes), 'Not enough voted Yes.');
    }else{
        require(totalVotes >= uint(A.getStatuteData('minAmountQuorumGeneralMeeting')), 'Quorum not reached.');
        if(votingType == VOTINGTYPE.APPOINTBOARDMEMBER || votingType == VOTINGTYPE.DISMISSBOARDMEMBER){
            require(voteYesCounter >= calcPercentOf(uint(A.getStatuteData('minPercentageChairmanVote')), totalVotes), 'Not enough voted Yes.');
        }else if(votingType == VOTINGTYPE.PURPOSECHANGE){
            require(voteYesCounter >= calcPercentOf(uint(A.getStatuteData('minPercentagePurposeVote')), A.amountAuthorizedMembers()), 'Not enough voted Yes.');
        }else if(votingType == VOTINGTYPE.STATUTECHANGE){
            require(voteYesCounter >= calcPercentOf(uint(A.getStatuteData('minPercentageStatuteVote')), totalVotes), 'Not enough voted Yes.');
        }else if(votingType == VOTINGTYPE.LIQUIDATION){
            require(voteYesCounter >= calcPercentOf(uint(A.getStatuteData('minPercentageLiquidationVote')), totalVotes), 'Not enough voted Yes.');
        }else if(votingType == VOTINGTYPE.NEWMEMBER){
            require(voteYesCounter >= calcPercentOf(uint(A.getStatuteData('minPercentageNewMemberVote')), totalVotes), 'Not enough voted Yes.');
        }
    }
    A.endMeeting(votingType);
}

}