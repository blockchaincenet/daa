Decentralized Autonomous Associations
=====================================

With this package we present the smart contracts for the implementation of an association in a blockchain system.
The current version of the smart contracts is implemented with Solidity and runs on the Ethereum network.

Feedback and suggestions for further development are welcome.
